import java.io.PrintStream;
import java.util.Random;

import aiproj.hexifence.Move;
import aiproj.hexifence.Piece;
import aiproj.hexifence.Player;
import aiproj.hexifence.mlumley.Board;
import aiproj.hexifence.mlumley.Coordinates;

public class RandomPlayer implements Piece, Player {

	int piece = 0;
	int oPiece = 0;
	Board board = null;
	boolean validMove = true;
	Random r = null;

	@Override
	public int init(int n, int p) {
		board = new Board(n);

		if (p == 1) {
			piece = Piece.BLUE;
			oPiece = Piece.RED;
		} else if (p == 2) {
			piece = Piece.RED;
			oPiece = Piece.BLUE;
		} else {
			return -1;
		}

		board.createBoard();
		r = new Random();

		return 0;
	}

	@Override
	public Move makeMove() {
		Move move = new Move();
		int row = r.nextInt(board.getLength());
		int col = r.nextInt(board.getLength());

		if(board.getBoard()[row][col] != '+'){
			while (board.getBoard()[row][col] != '+') {
				row = r.nextInt(board.getLength());
				col = r.nextInt(board.getLength());
			}
		}

		move.P = piece;
		move.Row = row;
		move.Col = col;

		board.setChar(move, piece);

		board.didCapture(new Coordinates(move.Row, move.Col), piece);
		return move;
	}

	@Override
	public int opponentMove(Move m) {
		if (board.getValue(m) == '+') {
			board.setChar(m, oPiece);

			if (board.didCapture(new Coordinates(m.Row, m.Col), oPiece)) {
				return 1;
			}

			return 0;
		}
		validMove = false;
		return -1;
	}

	@Override
	public int getWinner() {
		int winner = 0;

		if (!validMove) {
			return Piece.INVALID;
		} else if ((winner = board.findWinner()) > 0) {
			return winner;
		}
		return 0;
	}

	@Override
	public void printBoard(PrintStream output) {
		for (char[] line : board.getBoard()) {
			for (char ch : line) {
				System.out.print(ch + " ");
			}
			System.out.print("\n");
		}
	}

}
