import java.util.Comparator;
/*
 * COMP30024 Artificial Intelligence
 * HexiFence
 * Authors: Bikram Bora (bbora) and Michael Lumley (mlumley)
 */

/**
 * Comparator for state objects sorts in descending order
 */
public class StateComparator implements Comparator<State> {

	@Override
	public int compare(State o1, State o2) {
		// Sorting in descending order
		if (o1.getScore() < o2.getScore())
			return 1;
		else if (o1.getScore() == o2.getScore())
			return 0;
		else
			return -1;
	}

}
