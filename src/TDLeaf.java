import java.io.PrintWriter;
import java.util.ArrayList;


public class TDLeaf {

	public double alpha = 20;
	public double bravo = 20;
	public double charlie = 0.1;
	public double delta = 10;

	public ArrayList<Double> rewardA = new ArrayList<Double>();
	public ArrayList<Double> rewardB = new ArrayList<Double>();
	public ArrayList<Double> rewardC = new ArrayList<Double>();
	public ArrayList<Double> rewardD = new ArrayList<Double>();

	private ArrayList<Double> difference = new ArrayList<Double>();
	public double lastMove = 0;
	
	public ArrayList<String> weights = new ArrayList<String>();
	

	public void calcDifference() {
		for (int i = 0; i < rewardA.size() - 3; i++) {
			difference.add(rewardA.get(i + 1) - rewardA.get(i) + rewardB.get(i + 1) - rewardB.get(i)
					+ rewardC.get(i + 1) - rewardC.get(i) + rewardD.get(i + 1) - rewardD.get(i));
		}
		difference.add(lastMove - rewardA.get(rewardA.size() - 1) + rewardB.get(rewardB.size() - 1)
				+ rewardC.get(rewardC.size() - 1) + rewardD.get(rewardD.size() - 1));
	}

	public void tdUpdate() {
		
		double rate = 0.2;

		alpha = alpha + rate * firstSum(rewardA);
		bravo = bravo + rate * firstSum(rewardB);
		charlie = charlie + rate * firstSum(rewardC);
		delta = delta + rate * firstSum(rewardD);
		
		
	}

	private double firstSum(ArrayList<Double> weight) {
		double sum = 0;

		for (int i = 0; i < rewardA.size() - 2; i++) {
			sum += diff(i, weight) * (secondSum(i));
		}
		return sum;
	}

	private double secondSum(int i) {
		double sum = 0;
		double lamda = 0.95;

		for (int j = 0; j < rewardA.size() - 2; j++) {
			sum += Math.pow(lamda, (j - i)) * difference.get(j);
		}
		return sum;
	}

	private double diff(int i, ArrayList<Double> weight) {
		int delta = 1;
		return (weight.get(i + delta) - weight.get(i)) / (double) delta;
	}
	
	public void printWeights(PrintWriter writer){
		for(String str : weights){
			writer.println(str);
		}
		writer.close();
	}

	public void clearWeights() {
		rewardA.clear();
		rewardB.clear();
		rewardC.clear();
		rewardD.clear();
	}

}
