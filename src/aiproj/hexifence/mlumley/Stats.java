/*
 * COMP30024 Artificial Intelligence
 * HexiFence
 * Authors: Bikram Bora (bbora) and Michael Lumley (mlumley)
 */
package aiproj.hexifence.mlumley;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Calculates and stores all stats for a board
 */
public class Stats {

	private Board board;

	/**
	 * Set the board to do stats on
	 * 
	 * @param board
	 *            the board to set
	 */
	public void setBoard(Board board) {
		this.board = board;
	}

	/**
	 * Returns the number of tiles that can be captured in one move
	 * 
	 * @return number of tiles
	 */
	public int numCapturableHexes() {
		int count = 0;
		for (Tile tile : board.getTiles()) {
			if (tile.numOpenEdges() == 1) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Returns the number of possible moves
	 * 
	 * @return number of moves
	 */
	public int numPossibleMoves() {
		int possibleMoves = 0;

		// Count the number of '+' on the board
		for (char[] line : board.getBoard()) {
			for (char ch : line) {
				if (ch == '+') {
					possibleMoves++;
				}
			}
		}
		return possibleMoves;
	}

	/**
	 * Finds the max number of tiles that can be captured with one move
	 * 
	 * @return the number of tiles (0, 1, 2)
	 */
	public int maxCapturableHexes() {
		HashSet<Coordinates> points = new HashSet<Coordinates>();
		ArrayList<Tile> capturableTiles = new ArrayList<Tile>();

		// Find all capturable tiles
		for (Tile tile : board.getTiles()) {
			if (tile.numOpenEdges() == 1) {
				capturableTiles.add(tile);
			}
		}

		if (capturableTiles.size() == 0) {
			return 0;
		} else {
			for (Tile tile : capturableTiles) {
				// If the point is already in the hashset then two tiles are
				// sharing an open edge
				if (points.addAll(tile.getPoints())) {
					return 2;
				}
			}
			return 1;
		}
	}

	public int tilesWithOneOpenEdges() {
		int count = 0;
		for (Tile tile : board.getTiles()) {
			if (tile.numOpenEdges() == 1) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Get the number of tiles with two open edges
	 * 
	 * @return the number of tiles
	 */
	public int tilesWithTwoOpenEdges() {
		int count = 0;
		for (Tile tile : board.getTiles()) {
			if (tile.numOpenEdges() == 2) {
				count++;
			}
		}
		return count;
	}

	public int tilesWithThreeOpenEdges() {
		int count = 0;
		for (Tile tile : board.getTiles()) {
			if (tile.numOpenEdges() == 3) {
				count++;
			}
		}
		return count;
	}

	public int tilesWithFourOpenEdges() {
		int count = 0;
		for (Tile tile : board.getTiles()) {
			if (tile.numOpenEdges() == 4) {
				count++;
			}
		}
		return count;
	}

	public int tilesWithFiveOpenEdges() {
		int count = 0;
		for (Tile tile : board.getTiles()) {
			if (tile.numOpenEdges() == 5) {
				count++;
			}
		}
		return count;
	}

	public int tilesWithSixOpenEdges() {
		int count = 0;
		for (Tile tile : board.getTiles()) {
			if (tile.numOpenEdges() == 6) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Get the number of tiles captured but have not been claimed. Need because
	 * the eval function in player doesn't know which move is being made
	 * 
	 * @return the number of tiles
	 */
	public int numCapturedTiles() {
		int count = 0;

		for (Tile tile : this.board.getTiles()) {
			if (tile.numOpenEdges() == 0 && board.getValue(tile.getCenter().getX(), tile.getCenter().getY()) == '-') {
				count++;
			}
		}
		return count;
	}

	public int blueCapture() {
		int blue = 0;
		for (Tile tile : board.getTiles()) {
			if (board.getValue(tile.getCenter().getX(), tile.getCenter().getY()) == 'b') {
				blue++;
			}
		}
		return blue;
	}

	public int redCapture() {
		int red = 0;
		for (Tile tile : board.getTiles()) {
			if (board.getValue(tile.getCenter().getX(), tile.getCenter().getY()) == 'r') {
				red++;
			}
		}
		return red;
	}
}