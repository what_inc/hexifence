/*
 * COMP30024 Artificial Intelligence
 * HexiFence
 * Authors: Bikram Bora (bbora) and Michael Lumley (mlumley)
 */
package aiproj.hexifence.mlumley;

import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

import aiproj.hexifence.Move;
import aiproj.hexifence.Piece;
import aiproj.hexifence.Player;

/**
 * AI player for HexiFence using Negamax with alpha-beta pruning and move
 * ordering
 */
public class Mlumley implements Player, Piece {

	// Constants
	private static final int MAX_SEARCH_DEPTH = 4;
	private static final int MAX_DISTANCE_FROM_ROOT = 80;
	private static final int MAX_KILLER_MOVES_PER_DEPTH = 2;
	private static final double[] WEIGHTS = { 0.7923599767959005, 0.8971702924986382, 0.7000061491393125,
			0.3997452236233093, 0.5979885161887087, 0.7784259297804403, 0.39398989679959484, 0.0678380330637075,
			0.6806727469862189 };

	// Object variables
	private int myPiece = 0;
	private int opponentPiece = 0;
	private Board board = null;
	private boolean validMove = true;
	private Stats stats = new Stats();
	private Move bestMove = null;
	private Move[][] killerMoves = new Move[MAX_DISTANCE_FROM_ROOT][MAX_KILLER_MOVES_PER_DEPTH];
	private int ply = 0;

	@Override
	public int init(int n, int p) {
		board = new Board(n);
		board.createBoard();
		ply = 0;

		if (p == 1) {
			myPiece = Piece.BLUE;
			opponentPiece = Piece.RED;
		} else if (p == 2) {
			myPiece = Piece.RED;
			opponentPiece = Piece.BLUE;
		} else {
			return -1;
		}
		return 0;
	}

	@Override
	public Move makeMove() {

		negamax(this.board, myPiece, MAX_SEARCH_DEPTH, ply, -Double.MAX_VALUE, Double.MAX_VALUE);
		ply++;

		board.setChar(this.bestMove, this.myPiece);
		board.didCapture(new Coordinates(this.bestMove.Row, this.bestMove.Col), myPiece);

		return this.bestMove;
	}

	@Override
	public int opponentMove(Move m) {
		if (board.getValue(m) == '+') {
			board.setChar(m, this.opponentPiece);
			if (board.didCapture(new Coordinates(m.Row, m.Col), opponentPiece)) {
				return 1;
			}
			return 0;
		}
		validMove = false;
		return -1;
	}

	@Override
	public int getWinner() {
		int winner = 0;

		if (!validMove) {
			return Piece.INVALID;
		} else if ((winner = board.findWinner()) > 0) {
			return winner;
		}
		return 0;
	}

	@Override
	public void printBoard(PrintStream output) {
		for (char[] line : board.getBoard()) {
			for (char ch : line) {
				System.out.print(ch + " ");
			}
			System.out.print("\n");
		}
	}

	/**
	 * Depth limited negamax with alpha-beta pruning and move ordering. The best
	 * move is stored in bestMove, an object variable
	 * 
	 * @param board
	 *            the board
	 * @param piece
	 *            the player
	 * @param depth
	 *            the cutoff depth
	 * @param ply
	 *            the distance from the root
	 * @param alpha
	 *            the alpha value
	 * @param beta
	 *            the beta value
	 * @return the best score
	 */
	private double negamax(Board board, int piece, int depth, int ply, double alpha, double beta) {

		List<Move> nextMoves = generateMoves(board, piece);
		double score;
		double bestScore = -Double.MAX_VALUE;

		// Reached end of tree or cutoff depth
		if (depth == 0 || nextMoves.isEmpty()) {
			score = evaluateBoard(board, piece, WEIGHTS);
			return score;
		}

		nextMoves = orderMoves((LinkedList<Move>) nextMoves, ply);

		// For each child move calculate its score
		for (Move move : nextMoves) {
			board.setChar(move, piece);
			board.didCapture(new Coordinates(move.Row, move.Col), piece);
			if (piece == this.myPiece) {
				score = -negamax(board, opponentPiece, depth - 1, ply + 1, -beta, -alpha);
			} else {
				score = -negamax(board, myPiece, depth - 1, ply + 1, -beta, -alpha);
			}

			// Undo the move so that other moves can be made
			board.undoMove(move);

			if (bestScore < score) {
				bestScore = score;
				this.bestMove = move;
			}
			alpha = Math.max(alpha, score);
			// Beta cutoff
			if (alpha >= beta) {
				this.addKillerMove(move, ply);
				return beta;
			}
		}
		return bestScore;
	}

	/**
	 * Adds a killer move at a certain distance from the root node
	 * 
	 * @param move
	 *            the move
	 * @param ply
	 *            the distance from the root node
	 */
	private void addKillerMove(Move move, int ply) {
		// If this ply is full remove the oldest
		for (int i = killerMoves[ply].length - 2; i >= 0; i--) {
			killerMoves[ply][i + 1] = killerMoves[ply][i];
		}
		// Add the new move at the front
		killerMoves[ply][0] = move;
	}

	/**
	 * Orders the moves so that the best moves are at the front
	 * 
	 * @param nextMoves
	 *            the list of moves
	 * @param ply
	 *            the current distance from the root node
	 * @return
	 */
	private List<Move> orderMoves(LinkedList<Move> nextMoves, int ply) {
		Move killerMove = null;

		// Look though nextMoves for killer moves
		for (int slot = 0; slot < killerMoves[ply].length; slot++) {
			if ((killerMove = killerMoves[ply][slot]) != null) {
				for (int i = 0; i < nextMoves.size(); i++)
					// Check if a move is a killer move
					if (nextMoves.get(i).Row == killerMove.Row && nextMoves.get(i).Col == killerMove.Col) {
						// Place it at the front of nextMoves
						Move temp = nextMoves.remove(i);
						nextMoves.addFirst(temp);
					}
			}
		}
		return nextMoves;
	}

	/**
	 * Evaluates a board assigning a score to it
	 * 
	 * @param board
	 *            the board
	 * @param piece
	 *            the player
	 * @param weights
	 *            the weights use to calculate scores
	 * @return The score for the board
	 */
	private double evaluateBoard(Board board, int piece, double[] weights) {
		double score = 0;
		stats.setBoard(board);

		score += weights[0] * stats.numCapturableHexes();
		score += weights[1] * stats.maxCapturableHexes();

		if (piece == Piece.BLUE) {
			score += weights[2] * stats.blueCapture();
			score += weights[3] * stats.redCapture();
		} else if (piece == Piece.RED) {
			score += weights[2] * stats.redCapture();
			score += weights[3] * stats.blueCapture();
		}

		score += weights[4] * stats.tilesWithTwoOpenEdges();
		score += weights[5] * stats.tilesWithThreeOpenEdges();
		score += weights[6] * stats.tilesWithFourOpenEdges();
		score += weights[7] * stats.tilesWithFiveOpenEdges();
		score += weights[8] * stats.tilesWithSixOpenEdges();

		return score;
	}

	/**
	 * Given a board determine all possible moves
	 * 
	 * @param board
	 *            the board
	 * @param piece
	 *            the player
	 * @return A list of moves
	 */
	private List<Move> generateMoves(Board board, int piece) {
		List<Move> moves = new LinkedList<Move>();

		// Look for open edges in the board
		for (int i = 0; i < board.getLength(); i++) {
			for (int j = 0; j < board.getLength(); j++) {
				// Create the move and add it to a list of moves
				if (board.getValue(i, j) == '+') {
					Move move = new Move();
					if (piece == Piece.BLUE)
						move.P = Piece.BLUE;
					else
						move.P = Piece.RED;
					move.Row = i;
					move.Col = j;
					moves.add(move);
				}
			}
		}
		return moves;
	}

}
