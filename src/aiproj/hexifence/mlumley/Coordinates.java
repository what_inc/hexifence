/*
 * COMP30024 Artificial Intelligence
 * HexiFence
 * Authors: Bikram Bora (bbora) and Michael Lumley (mlumley)
 */
package aiproj.hexifence.mlumley;

/**
 * Represents an XY Coordinate. This class provides several useful methods for
 * representing and manipulating Coordinates.The object is mutable and it's
 * prime purpose is to act as a collection of two double values which are bound
 * together and travel as a single unit
 */
public class Coordinates {
	private int x;
	private int y;

	/**
	 * Create a Coordinates object with default value of x and y set to 0.
	 */
	public Coordinates() {
		this.x = 0;
		this.y = 0;
	}

	/**
	 * Create a Coordinates Object from 2 separate double values.
	 * 
	 * @param x
	 *            The X coordinate.
	 * @param y
	 *            The Y coordinate.
	 */
	public Coordinates(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Constructor which creates a Coordinates Object from another Coordinates
	 * object Essentially a copy constructor.
	 * 
	 * @param pos
	 *            The incoming coordinate object which is cloned.
	 */
	public Coordinates(Coordinates pos) {
		this.x = pos.getX();
		this.y = pos.getY();
	}

	/**
	 * Changes the values of the X and Y coordinates to . the incoming double
	 * arguments
	 * 
	 * @param x
	 *            The incoming value which x will be set to.
	 * @param y
	 *            The incoming value which y will be set to.
	 */
	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Changes the values of the X and Y coordinates to . the value of x and y
	 * of the incoming Coordinates Object
	 * 
	 * @param newPos
	 *            The incoming coordinate object whose x and y values are copied
	 *            into the x and y of the calling object.
	 */
	public void setXY(Coordinates newPos) {
		this.x = newPos.getX();
		this.y = newPos.getY();

	}

	/**
	 * Changes the values of the X coordinate to . the incoming double argument
	 * x
	 * 
	 * @param x
	 *            The incoming value which x of the calling Object will be set
	 *            to.
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Changes the values of the Y coordinate to . the incoming double argument
	 * y
	 * 
	 * @param y
	 *            The incoming value which y of the calling Object will be set
	 *            to.
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Returns the X coordinate of the calling Object
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * Returns the Y coordinate of the calling Object
	 */
	public int getY() {
		return this.y;
	}

	public boolean equals(Coordinates cood) {
		if (this.x == cood.x && this.y == cood.y) {
			return true;
		} else {
			return false;
		}
	}
}
